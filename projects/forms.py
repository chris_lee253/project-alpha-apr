from django import forms
from projects.models import Project


class ProjectForm(
    forms.ModelForm
):  # we define a django model form class by writing our own class that inherits from the django.forms.ModelForm class
    class Meta:  # need to create an inner class named Meta since it needs to interact with another model
        model = (
            Project  # We specify which Django model it should work/pull from.
        )
        fields = [  # specify which fields we want to show.
            "name",
            "description",
            "owner",
        ]
