from django import forms
from tasks.models import Task


class TaskForm(
    forms.ModelForm
):  # we define a django model form class by writing our own class that inherits from the django.forms.ModelForm class
    class Meta:  # need to create an inner class named Meta since it needs to interact with another model
        model = Task  # We specify which Django model it should work/pull from.
        fields = [  # specify which fields we want to show.
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
